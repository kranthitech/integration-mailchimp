var unirest = require("unirest")
var bluebird = require("bluebird")
var utils = require("../../utils")
authorizedRequest = utils.authorizedRequest

function _fetchLists(credential_instances, done) {
    var D = bluebird.pending()

    var lists = []
    console.log('in fetchLists with ', credential_instances)

    //Each provider can have only 1 instance, so we use the first item of the array
    var mailchimp_instance = credential_instances
    if (!mailchimp_instance || !mailchimp_instance.profile) {
        D.reject('Could not get lists for mailchimp')
        return D.promise
    }
    var api_endpoint = mailchimp_instance.profile.api_endpoint
    var lists_endpoint = api_endpoint + '/3.0/lists'
    console.log('api_endpoint- ' + api_endpoint)

    authorizedRequest(mailchimp_instance, '/lists?count=50', 'get')
        .then(function(body) {
            if (body.lists) {
                console.log('in body.lists')
                var lists = body.lists.map(function(list) {
                    var temp = { id: list.id, name: list.name }
                    return temp;
                })
                console.log('lists are ', lists)
                console.log('resolving with the above lists')

                D.resolve(lists)
            } else {
                 D.reject('Could not get lists')

            }
        })
    return D.promise
}


module.exports = {
    fetch: _fetchLists
}
