var bluebird = require("bluebird")
var utils = require("../../utils")
authorizedRequest = utils.authorizedRequest

function _addSubscriber(instance, email, name, widget_id, additional_information, done) {
    console.log('in _addSubscriber mailchimp with  email, name, widget_id, additional_information ', email, name, widget_id, additional_information)
    var D = bluebird.pending()
    var list_id = additional_information.provider_list_id
    var mailchimp_instance = instance
    var api_endpoint = mailchimp_instance.profile.api_endpoint
    var add_subscriber_stub = '/lists/' + list_id + '/members'
    var verify = additional_information.verify

    if( verify == null){
        verify = false
    }

    var subscriber_json = {
        email_address: email,
        merge_fields: {},
        status: "subscribed"
    }

    if (verify == true) {
        subscriber_json.status = "pending"
    }

    if (name) {
        subscriber_json.merge_fields.FNAME = name
    }
    authorizedRequest(mailchimp_instance, add_subscriber_stub, 'post', subscriber_json)
        .then(function(body) {
            console.log('Got response for addSubscriber mailchimp')
                console.log(body)
            if (body.id) {
                // D.resolve('CREATED_SUBSCRIPTION')
                done(null, 'CREATED_SUBSCRIPTION')
            } else {
                // D.resolve('SUBSCRIPTION_ERROR')
                done(null, 'SUBSCRIPTION_ERROR')
                // console.log(body)
            }

        })

    done(null, 'SUBSCRIPTION_ERROR')

    // return D.promise
}

function _subscriberExists(widget_id, email, list) {
    // /lists/{list_id}/members/{subscriber_hash}
    var D = bluebird.pending()
    console.log('in subscriberExists')

    var subscriber_hash = md5(email.toLowerCase().trim())
    var check_subscriber_stub = '/lists/' + list + '/members/' + subscriber_hash

    Mailchimp.findById(widget_id)
        .then(function(mailchimp_instance) {
            authorizedRequest(mailchimp_instance, check_subscriber_stub, 'get')
                .then(function(body) {
                    console.log('Got response for subscriberExists')
                        console.log(body)
                    if (body.id) {
                        console.log('subscriberExists - true')
                            //subscriber exists
                        D.resolve(true)
                    } else {
                        console.log('subscriberExists - false')
                        D.resolve(false)
                    }

                })
        })

    return D.promise
}


module.exports = {
    addSubscriber: _addSubscriber,
    subscriberExists: _subscriberExists

}
